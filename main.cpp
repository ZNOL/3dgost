#include <bits/stdc++.h>
using namespace std;

static unsigned int CURRENT_KEY = 0;

const vector<vector<int>> s_block = {
    {9, 6, 3, 2, 8, 11, 1, 7, 10, 4, 14, 15, 12, 0, 13, 5},
    {3, 7, 14, 9, 8, 10, 15, 0, 5, 2, 6, 12, 11, 4, 13, 1},
    {14, 4, 6, 2, 11, 3, 13, 8, 12, 15, 5, 10, 0, 7, 1, 9},
    {7, 5, 0, 13, 11, 6, 1, 2, 3, 10, 12, 15, 4, 14, 9, 8},
    {2, 7, 12, 15, 9, 5, 10, 11, 1, 4, 0, 13, 6, 8, 14, 3},
    {3, 10, 13, 12, 1, 2, 0, 11, 7, 5, 9, 4, 8, 15, 14, 6},
    {1, 13, 2, 9, 7, 10, 6, 0, 8, 12, 4, 5, 15, 3, 11, 14},
    {11, 10, 15, 5, 0, 12, 14, 8, 6, 2, 3, 9, 1, 7, 13, 4}
};

void fill(vector<vector<char>> &data)
{
    string s; cin >> s;

    int idx = 0;
    for (int i = 0; i < 8 && idx < s.length(); ++i) {
        for (int j = 0; j < 8 && idx < s.length(); ++j) {
            data[i][j] = s[idx++];
        }
    }
}

bitset<64> getSlice(vector<vector<char>> &cube, int x_start, int y_start, int z_start)
{
    bitset<64> output;

    int idx = 0;
    if (x_start != -1 && y_start == -1 && z_start == -1) {
        for (int y = 0; y < 8; ++y) {
            for (int z = 0; z < 8; ++z) {
                output[idx++] = cube[x_start][y] & (1LL << z);
            }
        }
    }
    else if (x_start == -1 && y_start != -1 && z_start == -1) {
        for (int x = 0; x < 8; ++x) {
            for (int z = 0; z < 8; ++z) {
                output[idx++] = cube[x][y_start] & (1LL << z);
            }
        }
    }
    else {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                output[idx++] = cube[x][y] & (1LL << z_start);
            }
        }
    }

    return output;
}

void printCube(vector<vector<char>> &cube)
{
    cout << "--------------------------------------\n";
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            cout << (int)cube[i][j] << " ";
            // cout << (char)abs(cube[i][j]);
        }
    }
    cout << "--------------------------------------\n";
}

void setSlice(vector<vector<char>> &cube, int x_start, int y_start, int z_start, bitset<64> input)
{
    int idx = 0;
    if (x_start != -1 && y_start == -1 && z_start == -1) {
        for (int y = 0; y < 8; ++y) {
            for (int z = 0; z < 8; ++z) {
                cube[x_start][y] = (cube[x_start][y] & (~(1LL << z)));
                cube[x_start][y] = (cube[x_start][y] | (input[idx++] << z));
            }
        }
    }
    else if (x_start == -1 && y_start != -1 && z_start == -1) {
        for (int x = 0; x < 8; ++x) {
            for (int z = 0; z < 8; ++z) {
                cube[x][y_start] = (cube[x][y_start] & (~(1LL << z)));
                cube[x][y_start] = (cube[x][y_start] | (input[idx++] << z));
            }
        }
    }
    else {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                cube[x][y] = (cube[x][y] & (~(1LL << z_start)));
                cube[x][y] = (cube[x][y] | (input[idx++] << z_start));
            }
        }
    }
}

unsigned long int getRandomInt()
{
    bitset<32> y;

    y[31] = CURRENT_KEY & 1;
    int a = bool(CURRENT_KEY & (1LL << 31));
    int b = bool(CURRENT_KEY & 1);
    y[30] = a ^ b;

    int idx = 30;
    for (int i = 29; i >= 0; --i) {
        if (i == 5 || i == 4) {
            a = bool(CURRENT_KEY & (1LL << idx--));
            y[i] = a ^ b;
        }
        else {
            y[i] = CURRENT_KEY & (1LL << idx--);
        }
    }
    CURRENT_KEY = y.to_ulong();

    return CURRENT_KEY;
}

void gost_round(bitset<32> &seq, bitset<32> &key)
{
    seq = seq | key;

    for (int i = 0; i < 8; ++i) {
        bitset<4> tmp;

        for (int j = 0; j < 4; ++j) {
            tmp[j] = seq[31 - 4 * i - j];
        }

        unsigned int x = s_block[i][tmp.to_ulong()];
        tmp = x;

        for (int j = 0; j < 4; ++j) {
            seq[31 - 4 * i - j] = tmp[j];
        }
    }

    bitset<32> save;
    for (int i = 31; i >= 21; --i) {
        save[i - 21] = seq[i];
    }
    seq = seq << 11;

    seq = seq | save;
}

pair<bitset<32>, bitset<32>> devide(bitset<64> &seq)
{
    pair<bitset<32>, bitset<32>> res;

    for (int i = 63; i >= 32; --i) {
        res.first[i - 32] = seq[i];
    }
    for (int i = 31; i >= 0; --i) {
        res.second[i] = seq[i];
    }

    return res;
}

void gost(bitset<64> &seq, bitset<32> &key)  // гост для ключа
{
    pair<bitset<32>, bitset<32>> data = devide(seq);

    for (int i = 0; i < 5; ++i) {
        gost_round(data.first, key);
        swap(data.first, data.second);
    }
    gost_round(data.first, key);

    for (int i = 63; i >= 0; --i) {
        if (32 <= i) {
            seq[i] = data.first[i - 32];
        }
        else {
            seq[i] = data.second[i];
        }
    }
}

void gost(bitset<64> &seq, bitset<64> &key)
{
    pair<bitset<32>, bitset<32>> data = devide(seq);
    pair<bitset<32>, bitset<32>> keys = devide(key);

    for (int i = 0; i < 4; ++i) {
        if ((i & 1) == 0) {
            gost_round(data.first, keys.first);
        }
        else {
            gost_round(data.first, keys.second);
        }
        swap(data.first, data.second);
    }
    gost_round(data.first, keys.second);
    swap(data.first, data.second);

    gost_round(data.first, keys.first);

    for (int i = 63; i >= 0; --i) {
        if (32 <= i) {
            seq[i] = data.first[i - 32];
        }
        else {
            seq[i] = data.second[i];
        }
    }
}

void GOST3D(vector<vector<char>> &cube, vector<vector<char>> &key)
{
    bitset<32> randomKey = getRandomInt();
    bitset<64> keyslice, slice;

    for (int x = 0; x < 8; ++x) {
        keyslice = getSlice(key, x, -1, -1);
        gost(keyslice, randomKey);
        randomKey = getRandomInt();

        slice = getSlice(cube, x, -1, -1);
        gost(slice, keyslice);

        setSlice(key, x, -1, -1, keyslice);
        setSlice(cube, x, -1, -1, slice);
    }

    for (int y = 0; y < 8; ++y) {
        keyslice = getSlice(key, -1, y, -1);
        gost(keyslice, randomKey);
        randomKey = getRandomInt();

        slice = getSlice(cube, -1, y, -1);
        gost(slice, keyslice);

        setSlice(key, -1, y, -1, keyslice);
        setSlice(cube, -1, y, -1, slice);
    }

    for (int z = 0; z < 8; ++z) {
        keyslice = getSlice(key, -1, -1, z);
        gost(keyslice, randomKey);
        randomKey = getRandomInt();

        slice = getSlice(cube, -1, -1, z);
        gost(slice, keyslice);

        setSlice(key, -1, -1, z, keyslice);
        setSlice(cube, -1, -1, z, slice);
    }
}


int main()
{
    // CURRENT_KEY = (unsigned)time(NULL);
    CURRENT_KEY = 123;

    vector<vector<char>> cube(8, vector<char>(8));
    vector<vector<char>> key(8, vector<char>(8));

    fill(cube);
    fill(key);

    printCube(cube);
    GOST3D(cube, key);
    printCube(cube);

    string s; cin >> s;

    return 0;
}
